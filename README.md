# Pokémon Team Builder

## Technologies
- Graph database using `neo4j`
- GraphQL API using Node.js + TypeScript
- Web app using React.js + TypeScript
